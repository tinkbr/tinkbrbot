# TinkBot

## Instalação
Para configurar o ambiente, basta clonar esse repositório e instalar as dependencias:

`pip install -r requirements.txt`

## Configuração
É preciso editar o arquivo de configuração `config.py` com suas credenciais do Telegram (token do bot) e com as credenciais do ZenDesk (dominio e oauth token).

Basta copiar o arquivo `config.py.sample` para um `config.py` já com as informações necessárias. 

## Executando
Para rodar o bot, basta rodar o script `tinkBot.py`:

`python tinkBot.py`

## OBSERVAÇÃO

O projeto está configurado para rodar com Python 3.6 ou superior.

---
Projeto desenvolvido para a Tink por Clara Nobre.