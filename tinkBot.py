# -*- coding: utf-8 -*-
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters,
                          ConversationHandler)
from zenpy.lib.api_objects import Ticket, User
from zenpy import Zenpy
from loguru import logger
import config

zenpy_client = Zenpy(**config.ZEN_CREDS)
EMAIL, PROBLEM, DONE, VERIFY, CLOSE = range(5)

@logger.catch
def start(bot, update):
    bot.send_message(
        chat_id=update.message.chat_id,
        text=(
            'Olá tudo bom? Eu sou o bot da Tink!'
            'Para que eu possa te ajudar precisarei de alguns dados, '
            'basta você digitar /ticket para iniciarmos o nosso atendimento. '
            'Se você quiser fechar algum ticket aberto digite /close '
            'Você pode cancelar essa conversa a qualquer momento '
            'digitando /cancel'
            )
    )

@logger.catch
def new_ticket(bot, update, user_data):
    user_data['user_info'] = update.message.from_user.to_dict()
    logger.info(f'User {user_data["user_info"]["id"]} start a new ticket')
    update.message.reply_text(
        'Vamos abrir um novo ticket.\n'
        'Primeiro, me fale qual o seu nome, por favor.'
    )
    return EMAIL

@logger.catch
def email(bot, update, user_data):
    user_data['name'] = update.message.text
    logger.info(f'User {user_data["user_info"]["id"]} set a name: {user_data["name"]}')
    update.message.reply_text(
        f'Tudo bem {user_data["name"]}?!\n'
        'nos informe o e-mail para contato por favor.'
    )
    return PROBLEM

@logger.catch
def problem(bot, update, user_data):
    user_data['email'] = update.message.text
    logger.info(f'User {user_data["user_info"]["id"]} set a email: {user_data["email"]}')
    update.message.reply_text(
        'Perfeito!\n'
        'Agora, queremos saber como podemos te ajudar.'
    )
    return DONE

# @logger.catch
# def verify(bot, update, user_data):
#     user_data['problem'] = update.message.text
#     update.message.reply_text(
#     'Por favor verifique os dados abaixo antes de enviar:\n'
#     f'Nome: {user_data["name"]}\n'
#     f'Email: {user_data["email"]}\n'
#     f'Problem: {user_data["problem"]}\n'
#     'Para confirmar o envio digite Ok\n'
#     'Para editar digite /cancel\n'
#     )
#     awnser = update.message.text
#     if awnser == "ok":
#         return DONE
#     else:
#         update.message.reply_text(
#         'Para confirmar o envio digite Ok\n'
#         'Para editar digite /cancel\n')

@logger.catch
def ticket_done(bot, update, user_data):
    ticket_number = create_ticket(user_data)
    #logger.info(f'User {user_data["user_info"]["id"]} set a problem: {user_data["problem"]}')
    update.message.reply_text(
        'Obrigado!\n'
        'Entraremos em contato o mais rápido possivel!\n'
        f'Você pode acompanhar seu ticket pelo Zendesk, anote o numero: {ticket_number}'
    )

    return ConversationHandler.END

@logger.catch
def create_ticket(user_data):
    ticket_audit = zenpy_client.tickets.create(
        Ticket(
            description=user_data['problem'],
            requester=User(
                    name=user_data['name'],
                    email=user_data['email']
                )
        ), async=False
    )
    return ticket_audit.ticket.id

# def button(bot, update):
#     query = update.callback_query
#
#     if query.data == "ok":
#         bot.answer_callback_query(
#             callback_query_id=query.id,
#             text=(
#                 (load_messages(
#                     update.message.chat_id, "rules_bit"))
#             ),
#             show_alert=True
#         )
#     elif query.data == "cancel":
#        bot.answer_callback_query(
#            callback_query_id=query.id
#        )

@logger.catch
def close_ticket(bot,update,user_data):
    ticket_status = zenpy_client.tickets.closed()

@logger.catch
def cancel(bot, update, user_data):
    logger.info(f'User {user_data["user_info"]["id"]} canceled a ticket')
    update.message.reply_text('Cancelado!')
    return ConversationHandler.END

@logger.catch
def helps(bot, update):
    bot.send_message(chat_id=update.message.chat_id, text='Help!')

@logger.catch
def echo(bot, update):
    bot.send_message(chat_id=update.message.chat_id, text=update.message.text)

@logger.catch
def error(bot, update):
    logger.warning(f'Update {bot} caused error {update.error}')

#@logger.catch
#def search(bot,update):
#    zenpy_client.search(type='ticket', status='open')

@logger.catch
def main():
    updater = Updater(config.TINK_BOT_TOKEN)
    dp = updater.dispatcher
    dp.add_handler(CommandHandler("start", start))
    #dp.add_handler(CommandHandler("close", close_ticket))
    dp.add_handler(CommandHandler("help", helps))
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('ticket', new_ticket, pass_user_data=True)],
        states={
            EMAIL: [MessageHandler(Filters.text, email, pass_user_data=True)],
            PROBLEM: [MessageHandler(Filters.text, problem, pass_user_data=True)],
            VERIFY: [MessageHandler(Filters.text, verify, pass_user_data=True)],
            #DONE: [MessageHandler(Filters.text, ticket_done, pass_user_data=True)]
            CLOSE: [MessageHandler(Filters.text, close_ticket, pass_user_data=True)]
        },
        fallbacks=[CommandHandler('cancel', cancel, pass_user_data=True)]
    )
    dp.add_handler(conv_handler)
    dp.add_handler(MessageHandler(Filters.text, echo))
    dp.add_error_handler(error)
    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
